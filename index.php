<? require_once 'connect.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>UsicMoney</title>
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	 <script src="js/jquery.min.js"></script>
	<!-- Custom Theme files -->
	 <link href="css/theme-style.css" rel='stylesheet' type='text/css' />
   	<!-- Custom Theme files -->
   	 <link rel="stylesheet" href="css/default.date.css" id="theme_date">
	<!-- Loading Bootstrap -->
	 <link href="css/vendor/bootstrap.min.css" rel="stylesheet">
	<!-- Loading Flat UI -->
	 <link href="css/flat-ui.css" rel="stylesheet">
	 <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!-- Hamburger -->
	 <link href="css/burger.css" rel="stylesheet">
</head>

<body>
<!---start-header-->
	<div class="header">	
		<div class="logo">
			<a href="#"><Image src="images/logo.png"></a>
		</div>
		<!--hamburger-->
	<div id="hamburger" class="hamburglar is-closed">

	    <div class="burger-icon">
	      <div class="burger-container">
	        <span class="burger-bun-top"></span>
	        <span class="burger-filling"></span>
	        <span class="burger-bun-bot"></span>
	      </div>
	    </div>
	    
	    <!-- svg ring containter -->
	    <div class="burger-ring">
	      <svg class="svg-ring">
		      <path class="path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="4" d="M 34 2 C 16.3 2 2 16.3 2 34 s 14.3 32 32 32 s 32 -14.3 32 -32 S 51.7 2 34 2" />
	      </svg>
	    </div>
	    <!-- the masked path that animates the fill to the ring -->
	    
	 		<svg width="0" height="0">
	       <mask id="mask">
	    <path xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#ff0000" stroke-miterlimit="10" stroke-width="4" d="M 34 2 c 11.6 0 21.8 6.2 27.4 15.5 c 2.9 4.8 5 16.5 -9.4 16.5 h -4" />
	       </mask>
	     </svg>
	    <div class="path-burger">
	      <div class="animate-path">
	        <div class="path-rotation"></div>
	      </div>
	    </div>
  	</div>
  	<!--/hamburger-->
		<!--start-top-nav-->
		<nav class="top-nav">
			<ul class="top-nav">
				<li id="enter" class="active"><a href="#" class="scroll" title="enter">Внести кошти</a></li>
				<li id="incas" class="page-scroll"><a href="#" class="scroll" title="incas">Інкасація</a></li>
			</ul>
			<a href="#" id="pull"><img src="images/nav-icon.png" title="menu" /></a>
		</nav>	
		<div class="clear"> </div>			
	</div>
<!---//End-header-->

<div class="container">
<!-- /nterPage -->
<div id="enterPage">
  	<!-- tabs -->
  	<div class="lastData" style="display:block">
	<ul id="tabs">
	    <li class="type choosetab printerTab" title="tab1">
	    	<a class="printer active" title="printer" href="#"><img src="images/Printer-32 (1).png"></a>
	    	<a class="printer passive" title="printer" href="#" style="display:none;"><img src="images/Printer-32.png"></a>
	    </li>
	    <li class="type choosetab teaTab" title="tab2">
	    	<a class="tea active" title="tea" href="#" style="display:none;"><img src="images/Beverage-Coffee-02-32 (1).png"></a>
			<a class="tea passive" title="tea" href="#"><img src="images/Beverage-Coffee-02-32.png"></a>
	    </li>
	    <li class="type type_desc"><span>Останні записи</span></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="printer last"> 
				<?php
					$result = mysql_query('SELECT * FROM Usic WHERE type="Принтер" ORDER BY id DESC LIMIT 1 ');
					echo '<table  class="lastPrinter last">';
					echo '<tbody>';
					while($data = mysql_fetch_array($result)){ 
						echo '<tr><td class="img_td"><img src="images/User-Login-32.png"></td><td><span>' . $data['name'] . ' '. $data['surname'] .'</span></td></tr>';
						echo '<tr><td class="img_td"><img src="images/Money-Bag-32.png"></td><td><span>' . $data['cash'] . ' грн.</span></td></tr>';
						echo '<tr><td class="img_td"><img src="images/Calendar-32.png"></td></td><td>' . $data['date'] . '</td></tr>';
					}
					echo '</tbody>';
					echo '</table>';
				?>
				<div class="more-link"><a class="btn btn-primary typebtn" href="details.php">Детально...</a></div>
			</div><!--/printer-->
		</div>

		<div id="tab2" style="display:none;">
			<div class="tea last"> 
				<?php
					$result = mysql_query('SELECT * FROM Usic WHERE type="Чай" ORDER BY id DESC LIMIT 1 ');
					echo '<table  class="lastPrinter last">';
					echo '<tbody>';
					while($data = mysql_fetch_array($result)){ 
						echo '<tr><td class="img_td"><img src="images/User-Login-32.png"></td><td><span>' . $data['name'] . ' '. $data['surname'] .'</span></td></tr>';
						echo '<tr><td class="img_td"><img src="images/Money-Bag-32.png"></td><td><span>' . $data['cash'] . ' грн.</span></td></tr>';
						echo '<tr><td class="img_td"><img src="images/Calendar-32.png"></td><td>' . $data['date'] . '</td></tr>';
					}
					echo '</tbody>';
					echo '</table>';
				?>
				<div class="more-link"><a class="btn btn-primary typebtn" href="details.php">Детально...</a></div>
			</div><!--/tea-->
		</div>
	</div>
	</div>
	<!-- /tabs -->

	<!-- form -->
	<div id="form">
	<div class="moneyIn enterMoney" style="display:block;">
		<form method="post" action="actionEnter.php" prc_adsf="true" class="mainMoney">
		<input type='hidden' name='action' value='add'/>
		<ul class="inputMoney" style="margin-bottom:0px; padding-left:0px;"> 
			<li><input type="text" value="" placeholder="*First name" required class="form-control" name="first_name" /></li>
			<li><input type="text" value="" placeholder="*Last name " required class="form-control" name="last_name" /></li>
			<li><input type="text" value="" placeholder="*Cash" required class="form-control" name="cash" /></li>
			<li>
  				<input type = "text"  id="text" value = "Принтер" style="display:none;" name = "type"/>
				<button id="printertype" type="button" onclick="document.getElementById('text').value='Принтер'" value="Принтер" class="btn btn-primary typebtn">Принтер</button>
            	<button id="teatype" type="button" onclick="document.getElementById('text').value='Чай'" value="Чай" value="Чай" class="btn btn-default typebtn">Чай</button>
			</li>
			<li style="display:none;"><input type="text" value="<?php echo date("Y-m-d H:i:s");?>" placeholder="Time " required class="form-control" name="time" /></li>
			<li><input type="text" value="" placeholder="Note" class="form-control" name="note" /></li>
			<li><center><input class="btn btn-success" type="submit" value="Ввести дані" style="width:100%;"></center></li>
		</ul>
	  	</form>
	</div>

	<div class="moneyIn incasMoney" style="display:none;">
		<form method="post" prc_adsf="true" class="mainMoney">
		<input type='hidden' name='action' value='add'/>
		<ul class="inputMoney" style="margin-bottom:0px; padding-left:0px;"> 
			<li><input type="text" value="" placeholder="*First name" required class="form-control" name="first_name" /></li>
			<li><input type="text" value="" placeholder="*Last name " required class="form-control" name="last_name" /></li>
			<li><input type="text" value="" placeholder="*Cash before" required class="form-control" name="cashbefore" /></li>
			<li><input type="text" value="" placeholder="*Cash after" required class="form-control" name="cash" /></li>
			<li>
  				<input type = "text"  id="text" value = "Принтер" style="display:none;" name = "type"/>
				<button id="printertype" type="button" onclick="document.getElementById('text').value='Принтер'" value="Принтер" class="btn btn-primary typebtn">Принтер</button>
            	<button id="teatype" type="button" onclick="document.getElementById('text').value='Чай'" value="Чай" value="Чай" class="btn btn-default typebtn">Чай</button>
			</li>
			<li style="display:none;"><input type="text" value="<?php echo date("Y-m-d H:i:s");?>" placeholder="Time " required class="form-control" name="time" /></li>
			<li><center><input class="btn btn-success" type="submit" value="Ввести дані" style="width:100%;"></center></li>
		</ul>
	  	</form>
	</div>
	</div>
	<!-- /form -->
</div>
<!-- /enterPage -->

<!-- fullInfo -->
<div id="showFull">
	<div class="types">
	    <div class="btn-group">
	        <button type="button" class="btn btn-success active" onclick="changeType()" value="printer">Принтер</button>
	        <button type="button" class="btn btn-success" onclick="changeType()" value="tea">Чай</button>
	    </div>
    </div>
	
	<div id="fullInfo">

	<div class="periods">
	    <div class="btn-group">
	        <button type="button" class="btn btn-primary active" value="Week">Тиждень</button>
	        <button type="button" class="btn btn-primary" value="Month">Місяць</button>
	        <button type="button" class="btn btn-primary" value="Year">Рік</button>
	    </div>
    </div>
    <div id="table">
		<div class="printerWeek">
		<?php
			$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 7 DAY)) and (type="Принтер")) ORDER BY id DESC');
			echo '<table  class="lastPrinter lastFull"><tbody>';
			echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
			while($data = mysql_fetch_array($result)){ 
				echo '<tr>';
				echo '<td>' . $data['name'] . '</td>';
				echo '<td>' . $data['surname'] . '</td>';
				echo '<td>' . $data['cash'] . ' грн. </td>';
				echo '<td>' . $data['date'] . '</td>';
				echo '<td>' . $data['note'] . '</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		?>
		</div>

		<div class="printerMonth">
		<?php
			$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 31 DAY)) and (type="Принтер")) ORDER BY id DESC');
			echo '<table  class="lastPrinter lastFull"><tbody>';
			echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
			while($data = mysql_fetch_array($result)){ 
				echo '<tr>';
				echo '<td>' . $data['name'] . '</td>';
				echo '<td>' . $data['surname'] . '</td>';
				echo '<td>' . $data['cash'] . ' грн. </td>';
				echo '<td>' . $data['date'] . '</td>';
				echo '<td>' . $data['note'] . '</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		?>
		</div>

		<div class="printerYear">
		<?php
			$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 365 DAY)) and (type="Принтер")) ORDER BY id DESC');
			echo '<table  class="lastPrinter lastFull"><tbody>';
			echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
			while($data = mysql_fetch_array($result)){ 
				echo '<tr>';
				echo '<td>' . $data['name'] . '</td>';
				echo '<td>' . $data['surname'] . '</td>';
				echo '<td>' . $data['cash'] . ' грн. </td>';
				echo '<td>' . $data['date'] . '</td>';
				echo '<td>' . $data['note'] . '</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		?>
		</div>

		<div class="teaWeek">
			<?php
				$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 7 DAY)) and (type="Чай")) ORDER BY id DESC');
				echo '<table  class="lastPrinter lastFull"><tbody>';
				echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
				while($data = mysql_fetch_array($result)){ 
					echo '<tr>';
					echo '<td>' . $data['name'] . '</td>';
					echo '<td>' . $data['surname'] . '</td>';
					echo '<td>' . $data['cash'] . ' грн. </td>';
					echo '<td>' . $data['date'] . '</td>';
					echo '<td>' . $data['note'] . '</td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '</table>';
			?>
		</div>

		<div class="teaMonth">
			<?php
				$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 31 DAY)) and (type="Чай")) ORDER BY id DESC');
				echo '<table  class="lastPrinter lastFull"><tbody>';
				echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
				while($data = mysql_fetch_array($result)){ 
					echo '<tr>';
					echo '<td>' . $data['name'] . '</td>';
					echo '<td>' . $data['surname'] . '</td>';
					echo '<td>' . $data['cash'] . ' грн. </td>';
					echo '<td>' . $data['date'] . '</td>';
					echo '<td>' . $data['note'] . '</td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '</table>';
			?>
		</div>

		<div class="teaYear">
			<?php
				$result = mysql_query('SELECT * FROM Usic WHERE ((date >= ( NOW() - INTERVAL 365 DAY)) and (type="Чай")) ORDER BY id DESC');
				echo '<table  class="lastPrinter lastFull"><tbody>';
				echo "<thead><th> Ім'я </th><th> Прізвище </th><th> Сума </th><th> Дата </th><th> Примітка </th></thead>";
				while($data = mysql_fetch_array($result)){ 
					echo '<tr>';
					echo '<td>' . $data['name'] . '</td>';
					echo '<td>' . $data['surname'] . '</td>';
					echo '<td>' . $data['cash'] . ' грн. </td>';
					echo '<td>' . $data['date'] . '</td>';
					echo '<td>' . $data['note'] . '</td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '</table>';
			?>
		</div>
	</div>
</div>
<!-- /fullInfo -->

</div>
<!--/Container-->

	<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
	<script>
	$(document).ready(function() {
		$("#tab2").hide(); // Initially hide all content
		$("#tabs .type:first").attr("id","current"); // Activate first tab

		$("#tabs .choosetab").click(function(){
	    	$("#tab1").hide(); //Hide all content
	    	$("#tab2").hide(); //Hide all content
	    	$("#"+$(this).attr('title')).fadeIn();
	    	$("#tabs li a").hide();
	    	$("#tabs li .passive").fadeIn();
	    	$("#tabs .printerTab").click(function(){
	    		$(".printerTab .passive").hide();
	    		$(".printerTab .active").fadeIn();
	    	})
	    	$("#tabs .teaTab").click(function(){
	    		$(".teaTab .passive").hide();
	    		$(".teaTab .active").fadeIn();
	    	})
	    	$("#tabs li").attr("id",""); //Reset id's
	    	$(this).attr("id","current"); // Activate this
	    })

	})();
	</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../dist/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../dist/js/flat-ui.min.js"></script>

    <script src="../assets/js/application.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
    	$("#table div").hide();
    	$("#table .printerWeek").fadeIn();

    	$(".types button").click(function(){
	    	if($(this).class=("btn btn-success")){
	    		$(".types button").attr("class","btn btn-success");
	    		$(this).addClass("active");
	    		$("#table div").hide();
	    		$("#table ."+$(".types .active").attr('value')+$(".periods .active").attr('value')).fadeIn();
	    	}
		})

		$(".periods button").click(function(){
		    	if($(this).class=("btn btn-primary")){
		    		$(".periods button").attr("class","btn btn-primary");
		    		$(this).addClass("active");
		    		$("#table div").hide();
		    		$("#table ."+$(".types .active").attr('value')+$(".periods .active").attr('value')).fadeIn();
		    	}
		})

    	$(".moneyIn button").click(function(){
    		$(".moneyIn button").attr("class","btn btn-default typebtn");
    		$(this).attr("class","btn btn-primary typebtn");
    	})

		$(".top-nav li a").click(function(){
	    	$(".top-nav li").attr("class","page-scroll");
	    	$(this).parent().attr("class","active");
	    	$("#form div").hide();
	    	$("#form ."+$(this).attr('title')+"Money").fadeIn();
	    })
	});

    
    //burger

	$('document').ready(function () {
	    var trigger = $('#hamburger'),
        isClosed = false;
        $("#showFull").hide();
   		trigger.click(function () {
    	burgerTime();
    });

    function burgerTime() {
      if (isClosed == true) {
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        $("#enterPage").fadeIn();
        $("#showFull").hide();
        isClosed = false;
      } else {
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        $("#enterPage").hide();
        $("#showFull").fadeIn();
        isClosed = true;
      }
    }
  });
    </script>

</body>
</html>
