 	$("#teatype").click(function(){
	    	$("#printertype").attr("class","btn btn-default typebtn");
	    	$(this).attr("class","btn btn-primary typebtn");
	})

	 $("#printertype").click(function(){
	    	$("#teatype").attr("class","btn btn-default typebtn");
	    	$(this).attr("class","btn btn-primary typebtn");
	})
    </script>

    <!-- burger -->
    <script type="text/javascript">
	    $('document').ready(function () {
	    var trigger = $('#hamburger'),
	        isClosed = true;

	    trigger.click(function () {
	      burgerTime();
	    });

	    function burgerTime() {
	      if (isClosed == true) {
	        trigger.removeClass('is-open');
	        trigger.addClass('is-closed');
	        $(".lastData").hide();
	        isClosed = false;
	      } else {
	        trigger.removeClass('is-closed');
	        trigger.addClass('is-open');
	        $(".lastData").fadeIn();
	        isClosed = true;
	      }
	    }
	  });

	function showEnter(){
		$("#incas").attr("class","page-scroll");
		$("#enter").attr("class","active");
		$("#form").load("pieces.php .enterMoney");
	}

	function showIncas(){
		$("#incas").attr("class","active");
		$("#enter").attr("class","page-scroll");
		$("#form").load("pieces.php .incasMoney");
	}
  